# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models
from django.contrib.auth.models import User as AuthUser
from django.contrib.auth.models import ContentType as DjangoContentType

#class AuthGroup(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    name = models.CharField(unique=True, max_length=240)
#    class Meta:
#        db_table = u'auth_group'
#
#class AuthUser(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    username = models.CharField(unique=True, max_length=90)
#    first_name = models.CharField(max_length=90)
#    last_name = models.CharField(max_length=90)
#    email = models.CharField(max_length=225)
#    password = models.CharField(max_length=384)
#    is_staff = models.IntegerField()
#    is_active = models.IntegerField()
#    is_superuser = models.IntegerField()
#    last_login = models.DateTimeField()
#    date_joined = models.DateTimeField()
#    class Meta:
#        db_table = u'auth_user'
#
#class AuthUserGroups(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    user = models.ForeignKey(AuthUser)
#    group = models.ForeignKey(AuthGroup)
#    class Meta:
#        db_table = u'auth_user_groups'
#
#
#class AuthMessage(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    user = models.ForeignKey(AuthUser)
#    message = models.TextField()
#    class Meta:
#        db_table = u'auth_message'
#
#class DjangoContentType(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    name = models.CharField(max_length=100)
#    app_label = models.CharField(unique=True, max_length=100)
#    model = models.CharField(unique=True, max_length=100)
#    class Meta:
#        db_table = u'django_content_type'
        
#class AuthPermission(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    name = models.CharField(max_length=150)
#    content_type = models.ForeignKey(DjangoContentType)
#    codename = models.CharField(unique=True, max_length=100)
#    class Meta:
#        db_table = u'auth_permission'
#
#class AuthGroupPermissions(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    group = models.ForeignKey(AuthGroup)
#    permission = models.ForeignKey(AuthPermission)
#    class Meta:
#        db_table = u'auth_group_permissions'
#        
#class AuthUserUserPermissions(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    user = models.ForeignKey(AuthUser)
#    permission = models.ForeignKey(AuthPermission)
#    class Meta:
#        db_table = u'auth_user_user_permissions'
#
#
#class DjangoSession(models.Model):
#    session_key = models.CharField(max_length=120, primary_key=True)
#    session_data = models.TextField()
#    expire_date = models.DateTimeField()
#    class Meta:
#        db_table = u'django_session'
#
#class DjangoSite(models.Model):
#    #id = models.IntegerField(primary_key=True)
#    domain = models.CharField(max_length=300)
#    name = models.CharField(max_length=150)
#    class Meta:
#        db_table = u'django_site'

class EstablishmentImages(models.Model):
    #id = models.IntegerField(unique=True)
    establishmentid = models.IntegerField(db_column='establishmentId') # Field name made lowercase.
    link = models.CharField(max_length=450)
    details = models.CharField(max_length=750, blank=True)
    class Meta:
        db_table = u'establishment_images'

class Establishments(models.Model):
    #id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=300, blank=True)
    address = models.CharField(max_length=450, blank=True)
    cp4 = models.IntegerField(null=True, blank=True)
    cp3 = models.IntegerField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    website = models.CharField(max_length=135, blank=True)
    phone = models.CharField(max_length=45, blank=True)
    creditsvalue = models.FloatField(null=True, db_column='creditsValue', blank=True) # Field name made lowercase.
    openinghours = models.CharField(max_length=1500, db_column='openingHours', blank=True) # Field name made lowercase.
    facebookinfo = models.CharField(max_length=300, db_column='facebookInfo', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'establishments'
        
class Products(models.Model):
    #id = models.IntegerField(primary_key=True)
    establishmentid = models.ForeignKey(Establishments, db_column='EstablishmentId') # Field name made lowercase.
    value = models.FloatField(null=True, db_column='Value', blank=True) # Field name made lowercase.
    referenceid = models.CharField(max_length=135, db_column='ReferenceId', blank=True) # Field name made lowercase.
    description = models.CharField(max_length=300, db_column='Description', blank=True) # Field name made lowercase.
    name = models.CharField(max_length=135, blank=True)
    class Meta:
        db_table = u'products'
        
class Giftstates(models.Model):
    #id = models.IntegerField(unique=True)
    statename = models.CharField(max_length=135, db_column='stateName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'giftStates'

class Gifts(models.Model):
    #id = models.IntegerField(unique=True)
    token = models.CharField(unique=True, max_length=100)
    description = models.CharField(max_length=135)
    productid = models.ForeignKey(Products,null=True, db_column='productId') # Field name made lowercase.
    datecreated = models.DateTimeField(null=True, db_column='dateCreated', blank=True) # Field name made lowercase.
    state = models.ForeignKey(Giftstates, null=True, db_column='state', blank=True)
    class Meta:
        db_table = u'gifts'

class Users(models.Model):
    #id = models.IntegerField(unique=True)
    name = models.CharField(max_length=135, blank=True)
    lastname = models.CharField(max_length=135, db_column='lastName', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=135, blank=True)
    phone = models.CharField(max_length=135, blank=True)
    creditsvalue = models.FloatField(null=True, db_column='creditsValue', blank=True) # Field name made lowercase.
    kudos = models.IntegerField(null=True, blank=True)
    address1 = models.CharField(max_length=1500, blank=True)
    cp = models.CharField(max_length=135, blank=True)
    cp2 = models.CharField(max_length=135, blank=True)
    totalkudos = models.IntegerField(null=True, db_column='totalKudos', blank=True) # Field name made lowercase.
    token = models.CharField(max_length=300, blank=True)
    class Meta:
        db_table = u'users'
        
class Usergiftrelationship(models.Model):
    #id = models.IntegerField(unique=True)
    receiverid = models.ForeignKey(Users, null=True, related_name='+', db_column='receiverId', blank=True) # Field name made lowercase.
    giftid = models.ForeignKey(Gifts, null=True, db_column='giftId', blank=True) # Field name made lowercase.
    senderid = models.ForeignKey(Users, null=True, related_name='+', db_column='senderId', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'userGiftRelationship'
        
class Actionslog(models.Model):
    #id = models.IntegerField(unique=True)
    userid = models.ForeignKey(Users, db_column='userId') # Field name made lowercase.
    action = models.CharField(max_length=135)
    description = models.CharField(max_length=900)
    timestamp = models.DateTimeField()
    class Meta:
        db_table = u'actionsLog'
        
class AuthUserType(models.Model):
    #id = models.IntegerField(unique=True)
    id_user = models.ForeignKey(Users, null=True, db_column='id_user', blank=True)
    id_establishment = models.ForeignKey(Establishments, null=True, db_column='id_establishment', blank=True)
    id_auth_user = models.ForeignKey(AuthUser, null=True, db_column='id_auth_user', blank=True)
    class Meta:
        db_table = u'auth_user_type'
        
class Receipts(models.Model):
    #id = models.IntegerField(unique=True)
    senderid = models.ForeignKey(Users, null=True, related_name='users_senderid', db_column='senderId', blank=True) # Field name made lowercase.
    receiverid = models.ForeignKey(Users, null=True, related_name='users_receiverid', db_column='receiverId', blank=True) # Field name made lowercase.
    amounttransfered = models.FloatField(null=True, db_column='amountTransfered', blank=True) # Field name made lowercase.
    kudosearned = models.IntegerField(null=True, db_column='kudosEarned', blank=True) # Field name made lowercase.
    giftid = models.ForeignKey(Gifts, null=True, db_column='giftId', blank=True) # Field name made lowercase.
    dateissued = models.DateTimeField(null=True, db_column='dateIssued', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'receipts'
        
class Plafondcodesstates(models.Model):
#    id = models.IntegerField(unique=True)
    statename = models.CharField(max_length=135, db_column='stateName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'plafondcodesstates'
        
class Plafondcodes(models.Model):
#    id = models.IntegerField(unique=True)
    token = models.CharField(max_length=135)
    value = models.FloatField()
    userid = models.ForeignKey(Users, null=True, db_column='userId', blank=True) # Field name made lowercase.
    state = models.ForeignKey(Plafondcodesstates, db_column='state')
    datecreated = models.DateTimeField(db_column='dateCreated') # Field name made lowercase.
    merchantid = models.ForeignKey(Establishments, db_column='merchantId') # Field name made lowercase.
    class Meta:
        db_table = u'plafondcodes'



class Plafondreceipts(models.Model):
#    id = models.IntegerField(unique=True)
    userid = models.ForeignKey(Users, db_column='userId') # Field name made lowercase.
    amounttransfered = models.FloatField(db_column='amountTransfered') # Field name made lowercase.
    merchantid = models.ForeignKey(Establishments, null=True, db_column='merchantId', blank=True) # Field name made lowercase.
    dateissued = models.DateTimeField(db_column='dateIssued') # Field name made lowercase.
    class Meta:
        db_table = u'plafondreceipts'
        
        
class Userrelationshiptype(models.Model):
    #id = models.IntegerField(unique=True)
    relationshipname = models.CharField(max_length=135, db_column='relationshipName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'userRelationshipType'

class Userrelationshipstatus(models.Model):
    #id = models.IntegerField(unique=True)
    statusname = models.CharField(max_length=135, db_column='statusName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'userRelationshipStatus'
        

        
class DjangoFacebookFacebookInvite(models.Model):
    #id = models.IntegerField(primary_key=True)
    updated_at = models.DateTimeField()
    created_at = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    user_invited = models.CharField(max_length=254, unique=True)
    message = models.TextField(blank=True)
    type = models.CharField(max_length=765, blank=True)
    wallpost_id = models.CharField(max_length=765, blank=True)
    error = models.IntegerField()
    error_message = models.TextField(blank=True)
    last_attempt = models.DateTimeField(null=True, blank=True)
    reminder_wallpost_id = models.CharField(max_length=765, blank=True)
    reminder_error = models.IntegerField()
    reminder_error_message = models.TextField(blank=True)
    reminder_last_attempt = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = u'django_facebook_facebook_invite'

class DjangoFacebookFacebooklike(models.Model):
    #id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField(unique=True)
    facebook_id = models.BigIntegerField(unique=True)
    name = models.TextField(blank=True)
    category = models.TextField(blank=True)
    created_time = models.DateTimeField(null=True, blank=True)
    class Meta:
        db_table = u'django_facebook_facebooklike'

class DjangoFacebookFacebookprofile(models.Model):
    #id = models.IntegerField(primary_key=True)
    about_me = models.TextField()
    facebook_id = models.BigIntegerField(unique=True, null=True, blank=True)
    access_token = models.TextField()
    facebook_name = models.CharField(max_length=765)
    facebook_profile_url = models.TextField()
    website_url = models.TextField()
    blog_url = models.TextField()
    image = models.CharField(max_length=765, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=3, blank=True)
    raw_data = models.TextField()
    user = models.ForeignKey(AuthUser, unique=True)
    class Meta:
        db_table = u'django_facebook_facebookprofile'

class DjangoFacebookFacebookuser(models.Model):
    #id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField(unique=True)
    facebook_id = models.BigIntegerField(unique=True)
    name = models.TextField(blank=True)
    gender = models.CharField(max_length=3, blank=True)
    class Meta:
        db_table = u'django_facebook_facebookuser'

class DjangoFacebookOpenGraphShare(models.Model):
    #id = models.IntegerField(primary_key=True)
    updated_at = models.DateTimeField()
    created_at = models.DateTimeField()
    user = models.ForeignKey(AuthUser)
    action_domain = models.CharField(max_length=765)
    facebook_user_id = models.BigIntegerField()
    share_dict = models.TextField(blank=True)
    content_type = models.ForeignKey(DjangoContentType, null=True, blank=True)
    object_id = models.IntegerField(null=True, blank=True)
    completed_at = models.DateTimeField(null=True, blank=True)
    error_message = models.TextField(blank=True)
    last_attempt = models.DateTimeField(null=True, blank=True)
    share_id = models.CharField(max_length=765, blank=True)
    class Meta:
        db_table = u'django_facebook_open_graph_share'
        
##Free Gifts

class Freecontenttype(models.Model):
    #id = models.IntegerField(unique=True, db_column='Id') # Field name made lowercase.
    contenttype = models.CharField(max_length=135, db_column='contentType', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'freeContentType'

class Freegifts(models.Model):
    #id = models.IntegerField(unique=True)
    giftid = models.ForeignKey(Gifts, db_column='giftId') # Field name made lowercase.
    latitude = models.FloatField()
    longitude = models.FloatField()
    class Meta:
        db_table = u'freeGifts'  

class Freecontent(models.Model):
    #id = models.IntegerField(unique=True, db_column='Id') # Field name made lowercase.
    content = models.CharField(max_length=3000, blank=True)
    typeid = models.ForeignKey(Freecontenttype, db_column='typeId') # Field name made lowercase.
    freegiftid = models.ForeignKey(Freegifts, db_column='freeGiftId') # Field name made lowercase.
    class Meta:
        db_table = u'freeContent'

class Giftsuserunknown(models.Model):
    #id = models.IntegerField(unique=True)
    giftid = models.ForeignKey(Gifts, null=True, db_column='giftId', blank=True) # Field name made lowercase.
    receivername = models.CharField(max_length=135, db_column='receiverName', blank=True) # Field name made lowercase.
    receiverfacebookid = models.CharField(max_length=135, db_column='receiverFacebookId', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'giftsUserUnknown'
