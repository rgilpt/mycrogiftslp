from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'MycrogiftsLP.views.home', name='home'),
    # url(r'^MycrogiftsLP/', include('MycrogiftsLP.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^$', 'MycrogiftsLP.views.index'),
    url(r'^admin/', include(admin.site.urls)),
    (r'^tourPressed/$', 'MycrogiftsLP.views.tourPressed'),
    (r'^facebookPressed/$', 'MycrogiftsLP.views.facebookPressed'),
    (r'^twitterPressed/$', 'MycrogiftsLP.views.twitterPressed'),
    (r'^homePressed/$', 'MycrogiftsLP.views.homePressed'),
    (r'^sendEmail/$', 'MycrogiftsLP.views.sendEmail'),
)
