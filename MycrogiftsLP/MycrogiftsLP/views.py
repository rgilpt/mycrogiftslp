'''
Created on Oct 29, 2012

@author: gabriela
'''
# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse

from MycrogiftsLP.utils import Utils


def index(request):
    return render_to_response('index.html',None, context_instance=RequestContext(request))

def tourPressed(request):
    # Default return list
    Utils.logAction(None, "UserPressedTour", {'description':'null'})
    return HttpResponse({}, mimetype='application/json')

def homePressed(request):
    # Default return list
    Utils.logAction(None, "UserPressedHome", {'description':'null'})
    return HttpResponse({}, mimetype='application/json')

def facebookPressed(request):
    # Default return list
    Utils.logAction(None, "UserPressedFacebook", {'description':'null'})
    return HttpResponse({}, mimetype='application/json')

def twitterPressed(request):
    # Default return list
    Utils.logAction(None, "UserPressedTwitter", {'description':'null'})
    return HttpResponse({}, mimetype='application/json')

def sendEmail(request):
    if 'email' in request.GET:
        email = request.GET["email"]
    else:
        email = 'anonymous'
    if 'emailContent' in request.GET:
        emailContent = request.GET["emailContent"]
    else:
        emailContent = ''
    Utils.logAction(None, "UserSentEmail", {
                                                 'email':email,
                                                 'emailContent':emailContent,
                                                 })
    return HttpResponse({}, mimetype='application/json')

