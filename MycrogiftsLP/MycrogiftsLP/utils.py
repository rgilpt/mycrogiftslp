'''
Created on May 28, 2012

@author: gabriela
'''

import re
from MycrogiftsLP.models import Actionslog, Users
#from mycrogifts.landing_page.models import Actionslog, Users

from datetime import datetime

class Utils(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
        
    #region Email Stuff
    def validateEmail(email):

        if len(email) > 7:
            if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
                return 1
        return 0
    validateEmail = staticmethod(validateEmail)
    #create Error log
    def logError(user, descriptionDictionary):
        action = Actionslog( 
                                action = "Error",
                                description = descriptionDictionary)
        if user:
            action.userid =user
        action.save()
    logError = staticmethod(logError)
    
    def logErrorByFunction(user,functionName,innerException):
        errordetails ={
                           "function": functionName,
                           "innerException": innerException
                           }
        action = Actionslog( 
                                action = "Error",
                                description = errordetails)
        if user:
            action.userid =user
        action.save()
    logErrorByFunction = staticmethod(logErrorByFunction)
    
    
            
    #create action
    def logAction(user,actionValue,descriptionDict):

        if not user:
            #check if user anonymous exists
            userAnonymous = Users.objects.filter(email = 'anonymous@mycrogifts.com')
            if not userAnonymous:
                newUser = Users(name = 'Anonymous', lastname = 'Anonymous', email = 'anonymous@mycrogifts.com')
                newUser.save()
                userAnonymous = newUser
            else:
                userAnonymous = userAnonymous[0]
            user = userAnonymous 
            
        
        action = Actionslog(userid = user, 
                                action = actionValue,
                                description = descriptionDict,
                                timestamp = datetime.now())
        action.save()
        return 0
    logAction = staticmethod(logAction)
    
#    def logAction(user,actionValue,descriptionPairs):
#
#        if not user:
#            #check if user anonymous exists
#            userAnonymous = Users.objects.filter(email = 'anonymous@mycrogifts.com')
#            if not userAnonymous:
#                newUser = Users(name = 'Anonymous', lastname = 'Anonymous', email = 'anonymous@mycrogifts.com')
#                newUser.save()
#                userAnonymous = newUser
#            else:
#                userAnonymous = userAnonymous[0]
#            user = userAnonymous 
#            
#        descriptionJson = None
#        if descriptionPairs:
#            descriptionJson = '{'
#            for descriptionPair in descriptionPairs:
#                descriptionJson = descriptionJson + descriptionPair
#            descriptionJson = descriptionJson + '}'
#        action = Actionslog(userid = user, 
#                                action = actionValue,
#                                description = descriptionJson)
#        action.save()
#        return 0
#    logAction = staticmethod(logAction)