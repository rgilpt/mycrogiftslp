'''
Created on Oct 17, 2012

@author: gabriela
'''
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.test.testcases import LiveServerTestCase

import time

import ast

from MycrogiftsLP.models import Actionslog

#import os
#os.environ['DJANGO_LIVE_TEST_SERVER_ADDRESS'] = 'localhost:8001'

class TestLandingPage(LiveServerTestCase):
    fixtures = ['admin_user.json']


    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)


    def tearDown(self):
        self.browser.quit()

    def testIfButtonTourExists(self):
        self.browser.get(self.live_server_url)
        #self.browser.get('localhost:8000')
        tourButton = self.browser.find_element_by_id('idTourLink')
        self.assertFalse(tourButton == None)
    
    def testIfSiteMapTourExists(self):
        self.browser.get(self.live_server_url)
        #self.browser.get('localhost:8000')
        tourButton = self.browser.find_element_by_id('idSiteMapTourLink')
        self.assertFalse(tourButton == None)
            
    def testIfSocialDivExists(self):
        self.browser.get(self.live_server_url)
        #self.browser.get('localhost:8000')
        socialDiv = self.browser.find_element_by_id('idSocial')
        self.assertFalse(socialDiv == None)
        
    def testIfFacebookDivExists(self):
        self.browser.get(self.live_server_url)
        #self.browser.get('localhost:8000')
        facebookDiv = self.browser.find_element_by_id('idFacebook')
        self.assertFalse(facebookDiv == None)
    
    def testPressSiteMapTourButton(self):
        
        self.browser.get(self.live_server_url)
        #user press button
        
        tourButton = self.browser.find_element_by_id('idSiteMapTourLink')
        tourButton.click()
#        
        time.sleep(1)
#        tourButton = self.browser.find_element_by_id('idTourLink')
#        tourButton.click()
        
        #check if tour button action is in database
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserPressedTour")
        
    def testPressSiteMapHomeButton(self):
        
        self.browser.get(self.live_server_url)
        #user press button
        
        tourButton = self.browser.find_element_by_id('idSiteMapHomeLink')
        tourButton.click()
#        
        time.sleep(1)
#        tourButton = self.browser.find_element_by_id('idTourLink')
#        tourButton.click()
        
        #check if tour button action is in database
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserPressedHome")
      
    def testPressTourButton(self):
        
        self.browser.get(self.live_server_url)
        #user press button
        
        tourButton = self.browser.find_element_by_link_text('TOUR')
        tourButton.click()
#        
        time.sleep(1)
#        tourButton = self.browser.find_element_by_id('idTourLink')
#        tourButton.click()
        
        #check if tour button action is in database
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserPressedTour")
        
    def testPressFacebook(self):
        
        self.browser.get(self.live_server_url)
        #user press button
        
        pressFacebook = self.browser.find_element_by_id('idFacebook')
        
        pressFacebook.click()
#        
        time.sleep(1)
#        tourButton = self.browser.find_element_by_id('idTourLink')
#        tourButton.click()
        
        #check if tour button action is in database
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserPressedFacebook")
        
    def testPressTwitter(self):
        
        self.browser.get(self.live_server_url)
        #user press button
        
        pressTwitter = self.browser.find_element_by_id('idTwitter')
        
        pressTwitter.click()
#        
        time.sleep(1)
#        tourButton = self.browser.find_element_by_id('idTourLink')
#        tourButton.click()
        
        #check if tour button action is in database
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserPressedTwitter")
        
    def testSendEmail(self):
        
        self.browser.get(self.live_server_url)
        
        #find button with id TellUs
        pressTellUs = self.browser.find_element_by_id("idTellUs")
        
        pressTellUs.click();
        
        #verify if a new form appears
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Email', body.text)
        
        #enter text for email
        username_field = self.browser.find_element_by_id('idEmail')
        username_field.send_keys('test@email.com')
        
        #enter text for content
        username_field = self.browser.find_element_by_id('idEmailContent')
        username_field.send_keys('I love Mycrogifts')
        
        #press send
        pressSendEmail = self.browser.find_element_by_id('idEmailSend')
        
        pressSendEmail.click()
#        
        time.sleep(1)
        
        #check if a message exists in actions with email and content
        actions = Actionslog.objects.all()
        self.assertTrue(len(actions) == 1,'The size is' + str( len(actions)))
        #check if a UserPressedTour action
        action = actions[0]
        self.assertTrue(action.action == "UserSentEmail")
        description = ast.literal_eval(action.description)
        
        self.assertEqual(description['email'], 'test@email.com', 'wrong email: ' + description['email']) 
        self.assertEqual(description['emailContent'], 'I love Mycrogifts', 'wrong content: ' + description['emailContent'])
        
        